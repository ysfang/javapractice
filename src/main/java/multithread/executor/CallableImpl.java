package multithread.executor;


import org.apache.commons.lang3.time.StopWatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CallableImpl {
    private static StopWatch stopwatch = new StopWatch();

    public static void main(String[] args) {
        initStopWatch();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
        List<Future<String>> resultList = new ArrayList<>();

//        for (int i = 0; i < 6; i++) { // all tasks would do in time.
        for (int i = 0; i < 10; i++) { // partial tasks wouldn't make it.
            Future<String> result = executor.submit(new CallableTask(i));
            resultList.add(result);
//            System.out.println("Thread-" + Thread.currentThread().getId() + " print: " + i);
        }

        /* shutdown() would finish tasks submitted then close.
         * once invoked, executor can no longer submit Callable tasks (or cause RejectedExecutionException).
         */
        executor.shutdown();

        try {
            executor.awaitTermination(3000, TimeUnit.MILLISECONDS);
            // if execution exceeds awaitTermination() above, terminate undone tasks and return task list not executed.
            List<Runnable> liNotRun = executor.shutdownNow();

            for(Future<String> future : resultList) {
                System.out.print(getStopWatch().toString() + " Thread-" + Thread.currentThread().getId() + ", isDone: " + future.isDone());
                // isDone() would be true due to normal termination, cancellation, exception
                if (future.isDone()) {
                    try {
                        System.out.print(", result: " + future.get());
                    } catch (ExecutionException executionException) {
                        // even task is done, getting result might still catch exception.
                        System.out.print(", exception occurs: " + executionException);
                    }
                }
                System.out.println("");
            }
        } catch (InterruptedException e) {
            // for awaitTermination()
            System.out.println("InterruptedException occurs, " + e);
        } finally {
            System.out.println("*** Executing other stuff....");
            stopwatch.stop();
        }
    }

    public static void initStopWatch() {
        stopwatch.start();
    }

    public static StopWatch getStopWatch() {
        return stopwatch;
    }
}
