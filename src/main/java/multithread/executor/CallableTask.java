package multithread.executor;

import java.util.concurrent.Callable;

import static multithread.executor.CallableImpl.getStopWatch;

public class CallableTask implements Callable {
    private int id;

    CallableTask(int id){
        this.id = id;
    }

    @Override
    public String call() throws Exception {
        work();
        String str = "EVEN";
        if (id % 2 == 1) {
            str = "ODD";
        }
        return id + "-" + str;
    }

    public void work() throws InterruptedException {
        System.out.println(getStopWatch().toString() + " Thread-" + Thread.currentThread().getId() + ", CallableTask: " + id + " is working...");
        Thread.sleep(1000);
//        System.out.println(getStopWatch().toString() + " Thread-" + Thread.currentThread().getId() + ", CallableTask: " + id + " has done working.");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
