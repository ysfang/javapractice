package dsa;


import org.apache.commons.lang3.time.StopWatch;

public class SumThree {
    public static void main(String[] args) {
        int[] ar = {30, -40, -20, -10, 40, 0, 10, 5};
        StopWatch stopwatch = new StopWatch();
        int n = ar.length;
        int count = 0;
        for (int i = 0; i < n - 2; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                for (int k = j + 1; k < n; k++) {
                    if (ar[i] + ar[j] + ar[k] == 0) {
                        count++;
                    }
                }
            }
        }
        System.out.println(count + ", time: " + stopwatch.toString());
    }
}
