package general.comparable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ComparableImpl {
    public static void main(String[] args) {
        List<Person> li = new ArrayList<>(Arrays.asList(new Person("Allen"), new Person("Bill")));
        Collections.sort(li);
        System.out.println("li = " + li);
    }

}
