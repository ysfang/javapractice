package general;

public class Impl {
    public static void main(String[] args) {
        Vendor[] vendors = Vendor.values();
        for (int i = 0; i < vendors.length; i++) {
            System.out.println(vendors[i].name());
        }
    }

    public enum Vendor {
        BOYA,
        TEST
    }
}
