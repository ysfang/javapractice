package general.floatdouble;

public class PrecisionError {
    public static void main(String[] args) {
//        floatError();
        doubleErrorWithMultiplcation();
        doubleErrorWithAdd();
        addError();
        addError2();
        doubleOverflow();
        loseSmall();
        floatToDoubleErr();
        devideByZero();
    }

    public static void floatError() {
        Float number1 = 1.89f;

        for (int i = 11; i < 800; i *= 2) {
            System.out.println("loop value: " + i);
            System.out.println(i * number1);
            System.out.println("");
        }
    }

    public static void doubleErrorWithMultiplcation() {
        double d = 0.3 * 3;
        System.out.println("Result of multiplication : " + d);
    }

    public static void doubleErrorWithAdd() {
        double total = 0;
        total += 5.6;
        total += 5.8;
        System.out.println("Result of add : " + total); // not 11.4
    }

    public static void addError() {
        System.out.println( 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f );
        double d = 0.1d + 0.1d + 0.1d + 0.1d + 0.1d + 0.1d + 0.1d + 0.1d + 0.1d + 0.1d;
        System.out.println(d);
        System.out.println(d==1);
        System.out.println(d==1.0);
    }

    public static void addError2() {
        System.out.println("0.1 + 0.1 + 0.1: " + (0.1 + 0.1 + 0.1));
        System.out.println("0.1 + 0.2: " + (0.1 + 0.2));
    }

    public static void doubleOverflow() {
        double big = 1.0e307 * 2000 / 2000;
        System.out.println("1.0e307 * 2000 / 2000 == 1.0e307: " + (big == 1.0e307));
    }

    public static void loseSmall() {
        System.out.println("1234.0d + 1.0e-13d == 1234.0d: " + (1234.0d + 1.0e-13d == 1234.0d));
    }

    public static void floatToDoubleErr() {
        System.out.println(Float.toString(0.1f));
        System.out.println(Double.toString(0.1f));
        System.out.println(Double.toString(0.1d));
    }

    public static void devideByZero() {
//        System.out.println(1 / 0); // results in java.lang.ArithmeticException
        System.out.println(22.0 / 0.0); // Infinity
        System.out.println(-13.0 / 0.0); // -Infinity
        System.out.println(0.0 / 0.0); // NaN
        System.out.println(22.0 / 0.0); // Infinity
        System.out.println(22.0 / -0.0); // -Infinity
        System.out.println(0.0 == -0.0); // float, double has a negative zero, the two zero values are equal.

    }
}
