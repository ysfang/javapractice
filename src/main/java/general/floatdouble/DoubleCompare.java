package general.floatdouble;

public class DoubleCompare {
    public static void main(String[] args) {
        testPrint();
    }

    public static void testPrint() {
        System.out.println("-0.0f == 0.0f: " + (-0.0f == 0.0f)); //true
        System.out.println("Float.compare(-0.0f, 0.0f): " + Float.compare(-0.0f, 0.0f)); //-1
        System.out.println("Float.NaN == Float.NaN: " + (Float.NaN == Float.NaN));//false
        System.out.println("Float.compare(Float.NaN, Float.NaN: " + (Float.compare(Float.NaN, Float.NaN))); //0
        System.out.println();
        System.out.println("-0.0d == 0.0d: " + (-0.0d == 0.0d)); //true
        System.out.println("Double.compare(-0.0d, 0.0d): " + Double.compare(-0.0d, 0.0d)); //-1
        System.out.println("Double.NaN == Double.NaN: " + (Double.NaN == Double.NaN));//false
        System.out.println("Double.compare(Double.NaN, Double.NaN: " + (Double.compare(Double.NaN, Double.NaN))); //0
    }

}
