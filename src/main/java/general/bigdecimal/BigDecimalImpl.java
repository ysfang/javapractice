package general.bigdecimal;

import java.math.BigDecimal;

public class BigDecimalImpl {
    public static void main(String[] args) {
        doubleToBigDecimal();
    }

    public static void doubleToBigDecimal() {
        System.out.println(new BigDecimal(2.2));
        System.out.println(new BigDecimal("2.2"));
        System.out.println(BigDecimal.valueOf(2.2));
    }
}
