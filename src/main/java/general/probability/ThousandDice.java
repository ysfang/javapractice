package general.probability;

import java.util.Random;

public class ThousandDice {
    public static void main(String[] args) {
        Random random = new Random();
        for (int times = 1; times <= 1000; times++) {
            long total = 0;
            for (int i = 0; i < 1000; i++) {
                long n = random.nextInt(999);
                total += (n==0) ? -1000L : 1L;
            }
            System.out.println("total: " + total);
        }
    }
}
