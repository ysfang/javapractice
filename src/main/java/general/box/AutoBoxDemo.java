package general.box;

public class AutoBoxDemo {
    public static void main(String[] args) {
        demo1();
        demo2();
        demo3();
    }

    private static void demo1() {
        Integer i1 = 100;
        Integer i2 = 100;

        if (i1 == i2)
            System.out.println("i1 == i2");
        else
            System.out.println("i1 != i2");

    }

    private static void demo2() {
        Integer i1 = 200;
        Integer i2 = 200;

        if (i1 == i2)
            System.out.println("i1 == i2");
        else
            System.out.println("i1 != i2");

    }

    private static void demo3() {
        Integer i1 = 200;
        Integer i2 = 200;

        if (i1.equals(i2))
            System.out.println("i1 == i2");
        else
            System.out.println("i1 != i2");

    }
}
