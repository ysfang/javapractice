package java8.lambda;

public class ThreadImpl {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Create Thread t1 with anonymous Inner Class");
            }
        });
        Thread t2 = new Thread(() -> System.out.println("Create Thread t2 with Lambda"));

        t1.start();
        t2.start();
    }
}
