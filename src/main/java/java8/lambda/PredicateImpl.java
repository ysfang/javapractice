package java8.lambda;

import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

public class PredicateImpl {
    public static void main(String[] args) {
        int[] li = {1, 2, 3, 4, 5};
        Predicate<Integer> pOdd = i -> i % 2 == 1;
        Predicate<Integer> pEven = i -> i % 2 == 0;

        System.out.println("Print odd numbers:");
        printByCriteria(li, pOdd);

        System.out.println("\nPrint even numbers:");
        printByCriteria(li, pEven);

        System.out.println("\nPrint inclusive numbers:");
        printByCriteria(li, pOdd.and(pEven));

    }

    public static void printByCriteria(int[] li, Predicate predicate) {
        for (int i = 0; i < li.length; i++) {
            if (predicate.test(Integer.valueOf(li[i]))) {
                System.out.println(li[i]);
            }
        }
    }
}
