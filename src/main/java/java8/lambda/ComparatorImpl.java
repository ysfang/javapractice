package java8.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorImpl {
    public static void main(String[] args) {
        List<Person> personList = Arrays.asList(new Person("Jack", "Fang"),
                new Person("Richard", "Fu"));

        // Sort with Inner Class
        Collections.sort(personList, new Comparator<Person>() {
            public int compare(Person p1, Person p2) {
                return p1.getSurName().compareTo(p2.getSurName());
            }
        });

        System.out.println("=== Sorted Asc SurName ===");
        for (Person p : personList) {
            p.printName();
        }

        // Use Lambda instead

        // Print Asc
        System.out.println("=== Sorted Asc SurName ===");
        Collections.sort(personList, (Person p1, Person p2) -> p1.getSurName().compareTo(p2.getSurName()));

        for (Person p : personList) {
            p.printName();
        }

        // Print Desc
        System.out.println("=== Sorted Desc SurName ===");
        Collections.sort(personList, (p1, p2) -> p2.getSurName().compareTo(p1.getSurName()));

        for (Person p : personList) {
            p.printName();
        }

    }
}

class Person {
    private String givenName;
    private String surName;
    private int age;
    //    private Gender gender;
    private String eMail;
    private String phone;
    private String address;

    public Person(String givenName, String surName) {
        this.givenName = givenName;
        this.surName = surName;
    }

    void printName() {
        System.out.println(givenName + ", " + surName);
    }

    public String getSurName() {
        return surName;
    }
}