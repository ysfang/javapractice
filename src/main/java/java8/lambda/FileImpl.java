package java8.lambda;

import java.io.File;
import java.util.Arrays;

public class FileImpl {
    public static void main(String[] args) {
        File directory = new File("./src/main/java/java8/lambda");

        String[] names = directory.list((dir, file) -> file.startsWith("Th"));

        System.out.println("names = " + Arrays.asList(names));
    }
}
