package java8.stream;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectionImpl {
    public static void main(String[] args) {
        setToList();
    }

    public static void setToList() {
        Set<String> set = new HashSet();
        set.add("1");
        set.add("2");
        set.add("3");

        List<Integer> li = set.stream()
                .map(Integer::valueOf)
                .map(it -> it * 10)
                .collect(Collectors.toList());
        li.stream().forEach(System.out::println);
    }
}
