package exceptionhandling;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
/*
* Handling ConcurrentModificationException with ListIterator.
* */
public class ImplWithListIterator {
    public static void main(String args[]) {
        iterateWithListIterator();

    }

    public static void iterateWithException() {
        List a1 = new ArrayList<String>();

        a1.add("List01");
        a1.add("List02");
        a1.add("List04");
        a1.add("List05");

        Iterator i1 = a1.iterator();
        while (i1.hasNext()) {
            Object obj = i1.next();
            if (obj.equals("List02")) {
                a1.add("List03");
            }
        }

        System.out.print("集合：\n\t" + a1 + "\n");
    }

    public static void iterateWithException2() {
        List<String> list = new ArrayList<String>();
        list.add("A");
        list.add("B");

        for (String s : list) {
            if (s.equals("B")) {
                list.remove(s);
            }
        }

        //foreach循环等效于迭代器
        /*Iterator<String> iterator=list.iterator();
        while(iterator.hasNext()){
            String s=iterator.next();
            if (s.equals("B")) {
                list.remove(s);
            }
        }*/
    }

    public static void iterateWithListIterator() {
        List a1 = new ArrayList<String>();

        a1.add("List01");
        a1.add("List");
        a1.add("List03");
        a1.add("List04");

        ListIterator l1 = a1.listIterator();
        while (l1.hasNext()) {
            Object obj = l1.next();
            if (obj.equals("List")) {
                l1.remove();
                l1.add("List02");
            }
        }
        System.out.print("集合：\n\t" + a1 + "\n");
    }
}
