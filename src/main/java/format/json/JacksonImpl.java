package format.json;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class JacksonImpl {
    private ObjectMapper objectMapper = ObjectMapperFactory.getInstance();

    public static void main(String[] args) throws IOException {
        JacksonImpl impl = new JacksonImpl();
        String strJsonObject = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
        String strJsonArray = "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";

        impl.strToJsonNode(strJsonObject);
        impl.strToJsonArray(strJsonArray);
    }

    public void strToObj(String str) throws IOException {
        Car car = objectMapper.readValue(str, Car.class);
        System.out.println("car = " + car);
    }

    public void strToJsonNode(String str) throws IOException {
        JsonNode jsonNode = objectMapper.readTree(str);
        String color = jsonNode.get("color").asText();
        System.out.println("color = " + color);
    }

    public void strToJsonArray(String str) throws IOException {
        List<Car> listCar = objectMapper.readValue(str, new TypeReference<List<Car>>(){});
        System.out.println("list size = " + listCar.size());
    }


}
