package format.json;

import java.io.IOException;
import java.util.List;

public class GeneralJsonNodeImpl {
    public static void main(String[] args) throws IOException {
        String strJsonObject = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
        String strJsonArray = "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";

        GeneralJsonNode node1 = new GeneralJsonNode(strJsonObject);
        System.out.println("node1.isObjectNode() = " + node1.isObjectNode());
        System.out.println("node1.isArrayNode() = " + node1.isArrayNode());
        System.out.println("node1.getData() = " + node1.getData());
        System.out.println("node1.getData().getClass() = " + node1.getData().getClass());
        System.out.println("node1.getDataByClass(Car) = " + node1.getDataByClass(Car.class));
        System.out.println("node1.getDataByClass(Car).getClass() = " + node1.getDataByClass(Car.class).getClass());

        GeneralJsonNode node2 = new GeneralJsonNode(strJsonArray);
        System.out.println("node2.isObjectNode() = " + node2.isObjectNode());
        System.out.println("node2.isArrayNode() = " + node2.isArrayNode());
        System.out.println("node2.getData() = " + node2.getData());
        System.out.println("node2.getData().getClass() = " + node2.getData().getClass());
        System.out.println("node2.getDataByClass(Car) = " + node2.getDataByClass(Car.class));
        System.out.println("node2.getDataByClass(Car).getClass() = " + node2.getDataByClass(Car.class).getClass());
        System.out.println("((List)node2.getDataByClass(Car.class)).get(0).getClass() = " + ((List)node2.getDataByClass(Car.class)).get(0).getClass());
    }
}
