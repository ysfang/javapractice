package format.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.List;

public class GeneralJsonNode {
    private ObjectMapper objectMapper = ObjectMapperFactory.getInstance();
    private JsonNode data;

    private GeneralJsonNode() {}
    public GeneralJsonNode(String str) throws IOException {
        data = objectMapper.readTree(str);
    }

    public boolean isArrayNode() {
        return data instanceof ArrayNode;
    }

    public boolean isObjectNode() {
        return data instanceof ObjectNode;
    }

    public JsonNode getData() {
        return data;
    }

    public Object getDataByClass(Class clazz) throws IOException {
        if (isObjectNode()) {
            return objectMapper.readValue(new TreeTraversingParser(data), clazz);
        } else {
            return objectMapper.readValue(new TreeTraversingParser(data), TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
        }
    }
}
