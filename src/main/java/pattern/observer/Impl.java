package pattern.observer;

public class Impl {
    public static void main(String[] args) {
        NewsPublisher discovery = new NewsPublisher();
        NewsReader jack = new NewsReader("Jack");
        discovery.register(jack);
        discovery.interviewNews("A missing dog is found.");

        TemperaturePublisher centralWeather = new TemperaturePublisher();
        TemperatureReader device = new TemperatureReader("hitachi-001");
        centralWeather.register(device);
        centralWeather.measureTemperature(8.2);
    }
}
