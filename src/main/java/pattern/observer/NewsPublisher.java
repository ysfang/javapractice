package pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class NewsPublisher implements Subject {
    List<Observer> observers = new ArrayList<>();
    private String news = "";

    @Override
    public void register(Observer observer) {
        if (!observers.contains(observer))
            observers.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        if (!observers.contains(observer))
            observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : observers)
            observer.update(news);
    }

    public void interviewNews(String str) {
        news = str;
        notifyObserver();
    }
}
