package pattern.observer;

public interface Observer<T> {
    void update(T observable);
}
