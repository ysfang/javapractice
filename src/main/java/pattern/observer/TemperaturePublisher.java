package pattern.observer;

import java.util.ArrayList;
import java.util.List;

public class TemperaturePublisher implements Subject {
    List<Observer> observers = new ArrayList<>();
    private double temperature = 0;

    @Override
    public void register(Observer observer) {
        if (!observers.contains(observer))
            observers.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        if (!observers.contains(observer))
            observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : observers)
            observer.update(temperature);
    }

    public void measureTemperature(double _temperature) {
        temperature = _temperature;
        notifyObserver();
    }
}
