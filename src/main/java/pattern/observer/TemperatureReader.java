package pattern.observer;

public class TemperatureReader implements Observer {
    private String id;
    public TemperatureReader(String str) {
        id = str;
    }

    @Override
    public void update(Object observable) {
        System.out.println("Temperature reader: " + this.id + " has got updated to: " + String.valueOf(observable));
    }
}
