package pattern.observer;

public class NewsReader implements Observer{
    private String name;
    public NewsReader(String str) {
        name = str;
    }

    @Override
    public void update(Object observable) {
        System.out.println("News reader: " + this.name + " has got news: " + observable);
    }
}
