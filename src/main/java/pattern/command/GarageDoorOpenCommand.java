package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class GarageDoorOpenCommand implements Command {
    private GarageDoor door;

    public GarageDoorOpenCommand(GarageDoor garageDoor) {
        door = garageDoor;
    }

    @Override
    public void execute() {
        door.open();
    }
}
