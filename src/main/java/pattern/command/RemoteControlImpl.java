package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class RemoteControlImpl {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();
        Light kitchenlight = new Light("The kitchen light");
        LightOnCommand lightOnCommand = new LightOnCommand(kitchenlight);
        LightOffCommand lightOffCommand = new LightOffCommand(kitchenlight);
        GarageDoor garageDoor = new GarageDoor("The garage door");
        GarageDoorOpenCommand garageDoorOpenCommand = new GarageDoorOpenCommand(garageDoor);
        GarageDoorOffCommand garageDoorOffCommand = new GarageDoorOffCommand(garageDoor);

        remoteControl.setCommand(0, lightOnCommand, lightOffCommand);
        remoteControl.setCommand(1, garageDoorOpenCommand, garageDoorOffCommand);
        remoteControl.onButtonWasPressed(0);
        remoteControl.offButtonWasPressed(0);
        remoteControl.onButtonWasPressed(1);
        remoteControl.offButtonWasPressed(1);
    }
}
