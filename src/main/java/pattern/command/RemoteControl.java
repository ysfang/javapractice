package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class RemoteControl {
    int SLOT_NUM = 7;
    Command[] onCommands;
    Command[] offCommands;

    public RemoteControl() {
        onCommands = new Command[SLOT_NUM];
        offCommands = new Command[SLOT_NUM];

        Command noCommand = new NoCommand();
        for (int i = 0 ; i < SLOT_NUM ; i++) {
            onCommands[i] = noCommand;
            offCommands[i] = noCommand;
        }
    }

    public void setCommand(int slot, Command onCommand, Command offCommand) {
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPressed(int slot) {
        onCommands[slot].execute();
    }

    public void offButtonWasPressed(int slot) {
        offCommands[slot].execute();
    }
}
