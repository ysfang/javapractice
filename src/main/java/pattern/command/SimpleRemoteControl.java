package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class SimpleRemoteControl {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void onButtonPressed() {
        command.execute();
    }
}
