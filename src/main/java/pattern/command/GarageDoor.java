package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class GarageDoor {
    private String name = "";
    public GarageDoor(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void open() {
        System.out.println(String.format("%s is open.", name));
    }

    public void close() {
        System.out.println(String.format("%s is closed.", name));
    }
}
