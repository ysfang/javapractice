package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public interface Command {
    public void execute();
}
