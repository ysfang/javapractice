package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class SimpleRemoteControlImpl {
    public static void main(String[] args) {
        SimpleRemoteControl simpleRemoteControl = new SimpleRemoteControl();
        Light livingroomLight = new Light("The livingroom light");
        LightOnCommand lightOnCommand = new LightOnCommand(livingroomLight);

        simpleRemoteControl.setCommand(lightOnCommand);
        simpleRemoteControl.onButtonPressed();

    }
}
