package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class NoCommand implements Command {
    @Override
    public void execute() {
        System.out.println("command not set");
    }
}
