package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class GarageDoorOffCommand implements Command {
    private GarageDoor door;

    public GarageDoorOffCommand(GarageDoor garageDoor) {
        door = garageDoor;
    }

    @Override
    public void execute() {
        door.close();
    }
}
