package pattern.command;

/**
 * Created by ysfang on 2017/4/5.
 */
public class Light {
    private String name = "";

    public Light(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void on() {
        System.out.println(String.format("%s is on.", name));
    }

    public void off() {
        System.out.println(String.format("%s is off.", name));
    }
}
