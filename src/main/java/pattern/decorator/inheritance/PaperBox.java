package pattern.decorator.inheritance;

/**
 * Created by ysfang on 2017/2/7.
 */
public class PaperBox extends CakeDecorator {

    PaperBox(Cake cake) {
        super(cake);
    }

    @Override
    String getDescription() {
        return super.cake.getDescription() + " wrapped with paper box";
    }

    @Override
    double cost() {
        return super.cake.cost() + 5;
    }
}
