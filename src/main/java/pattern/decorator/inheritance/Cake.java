package pattern.decorator.inheritance;

/**
 * Created by ysfang on 2017/2/7.
 */
public abstract class Cake {
    String description = "";

    String getDescription() {
        return description;
    }

    abstract double cost();
}
