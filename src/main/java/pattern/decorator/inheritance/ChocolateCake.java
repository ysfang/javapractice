package pattern.decorator.inheritance;

/**
 * Created by ysfang on 2017/2/7.
 */
public class ChocolateCake extends Cake {

    ChocolateCake() {
        description = "chocolate cake";
    }

    @Override
    double cost() {
        return 10;
    }
}
