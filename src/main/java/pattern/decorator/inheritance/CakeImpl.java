package pattern.decorator.inheritance;

/**
 * Created by ysfang on 2017/2/7.
 */
public class CakeImpl {
    public static void main(String[] args) {
        Cake chocolateCake = new ChocolateCake();
        System.out.println("Billing: " + chocolateCake.getDescription() + ", costs: " + chocolateCake.cost());

        Cake product = new PaperBox(chocolateCake);
        System.out.println("Billing: " + product.getDescription() + ", costs: " + product.cost());
    }
}
