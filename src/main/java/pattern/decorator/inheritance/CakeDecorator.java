package pattern.decorator.inheritance;

/**
 * Created by ysfang on 2017/2/7.
 */
public abstract class CakeDecorator extends Cake {
    protected Cake cake;

    CakeDecorator(Cake cake) {
        this.cake = cake;
    }

//    abstract String getDescription();
}
