package pattern.decorator.interfaxe;

/**
 * Created by ysfang on 2017/2/7.
 */
public class ProductImpl {
    public static void main(String[] args) {
        Product mayDayCd = new MayDayCD();
        System.out.println("Billing: " + mayDayCd.getDescription() + ", costs: " + mayDayCd.cost());

        Product xmasPromo = new XmasPromotion(mayDayCd);
        System.out.println("Billing: " + xmasPromo.getDescription() + ", costs: " + xmasPromo.cost());
    }
}
