package pattern.decorator.interfaxe;

/**
 * Created by ysfang on 2017/2/7.
 */
public interface Product {
    abstract String getDescription();
    abstract double cost();
}
