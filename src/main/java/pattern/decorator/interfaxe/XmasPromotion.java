package pattern.decorator.interfaxe;

/**
 * Created by ysfang on 2017/2/7.
 */
public class XmasPromotion extends PromotionDecorator {
    XmasPromotion(Product product) {
        super(product);
    }

    @Override
    public double cost() {
        return super.product.cost() / 2;
    }

    @Override
    public String getDescription() {
        return super.product.getDescription() + " on sale";
    }
}
