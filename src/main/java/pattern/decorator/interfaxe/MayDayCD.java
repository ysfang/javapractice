package pattern.decorator.interfaxe;

/**
 * Created by ysfang on 2017/2/7.
 */
public class MayDayCD implements Product {

    @Override
    public String getDescription() {
        return "May Day CD";
    }

    @Override
    public double cost() {
        return 300;
    }
}
