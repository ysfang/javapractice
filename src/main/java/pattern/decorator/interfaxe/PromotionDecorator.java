package pattern.decorator.interfaxe;

/**
 * Created by ysfang on 2017/2/7.
 */
public abstract class PromotionDecorator implements Product {
    Product product;

    PromotionDecorator(Product product) {
        this.product = product;
    }
}
