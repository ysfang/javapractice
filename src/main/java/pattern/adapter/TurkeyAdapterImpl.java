package pattern.adapter;

public class TurkeyAdapterImpl {
    public static void main(String[] args) {
        Turkey wildTurkey = new WildTurkey();
        Duck duck = new TurkeyAdapter(wildTurkey);
        testDuck(duck);
    }

    public static void testDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }
}
