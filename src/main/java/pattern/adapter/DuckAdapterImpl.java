package pattern.adapter;

public class DuckAdapterImpl {
    public static void main(String[] args) {
        Duck duck = new MallardDuck();
        Turkey turkey = new DuckAdapter(duck);
        testTurkey(turkey);
    }

    public static void testTurkey(Turkey turkey) {
        turkey.gobble();
        turkey.fly();
    }
}
