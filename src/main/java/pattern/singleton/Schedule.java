package pattern.singleton;

import java.util.HashMap;
import java.util.Map;

public class Schedule {
    private String name;
    private String task;
    private static Map nameTaskMap = new HashMap();

    private Schedule() { }

    public static String getScheduleByName(String name) {
        return nameTaskMap.get(name).toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

}
