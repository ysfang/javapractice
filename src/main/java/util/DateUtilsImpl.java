package util;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class DateUtilsImpl {
    public static void main(String[] args) {
        Date date = DateUtils.addDays(new Date(), 1);
        System.out.println(date);
        date = DateUtils.truncate(date, Calendar.DATE);
        System.out.println(date);
    }

}
